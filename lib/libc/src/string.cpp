#include "string.h"
#include <durgos/string.hpp>
#include <stdint.h>

LIBC_START_HEADER

size_t strlen(const char* str)
{
    size_t size = 0;
    while ( str[size] )
        size++;
    return size;
}

int strcmp(const char* lhs, const char* rhs)
{
    return durgos::strcmp(lhs, rhs);
}

int strncmp(const char* lhs, const char* rhs, size_t count)
{
    return durgos::strncmp(lhs, rhs, count);
}

void* memset(void* s, int c, size_t n)
{
    byte *str = (byte*)s;
    while (n--)
        *str++ = c;
    return s;
}

void* memcpy(void* dest, const void* src, size_t n)
{
    durgos::pod_copy((const byte*)src, (byte*)dest, n);
    return dest;
}

void* memmove(void* dest, const void* src, size_t n)
{
    byte *pd = (byte*)dest;
    const byte *ps = (const byte*)src;
    if (ps < pd)
    {
        // Copy in reverse order to avoid overwriting
        for (pd += n, ps += n; n--;)
            *--pd = *--ps;
    }
    else
    {
        durgos::pod_copy(ps, pd, n);
    }

    return dest;
}

int memcmp(const void* s1, const void* s2, size_t n)
{
    const byte* p1 = (const byte*)s1;
    const byte* p2 = (const byte*)s2;
    while(n--)
    {
        if( *p1 != *p2 )
            return *p1 - *p2;

        p1++;
        p2++;
    }
    return 0;
}

LIBC_END_HEADER
