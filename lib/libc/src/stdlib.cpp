#include <stdlib.h>
#include <new>

LIBC_START_HEADER

void* malloc(size_t size)
{
    return operator new(size, std::nothrow);
}

void free(void *ptr)
{
    return operator delete(ptr);
}

LIBC_END_HEADER
