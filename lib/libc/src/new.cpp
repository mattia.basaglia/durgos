#include <new>
#include <durgos/memory.hpp>

namespace std {
  const nothrow_t nothrow;
} // namespace std


struct Block
{
    Block* next;
    bool used;
    std::size_t size;

    void* memory() const
    {
        return (void*)(this+1);
    }

    bool available(std::size_t sz) const
    {
        return !used && size >= sz;
    }

    void expand()
    {
        if ( next && !next->used && (char*)next == (char*)memory() + size )
        {
            size += next->size + sizeof(Block);
            next = next->next;
        }
    }

    /**
     * \pre used == false && new_size < size + sizeof(Block)
     */
    void split(std::size_t new_size)
    {
        Block* after = (Block*)((char*)memory() + new_size);
        after->size = size - new_size - sizeof(Block);
        after->next = next;
        after->used = false;
        size = new_size;
        next = after;
    }
};

Block pool_head = {nullptr, false, 0};

void* operator new(std::size_t size, const std::nothrow_t&) noexcept
{
    Block* block = &pool_head;
    while ( block->next && !block->available(size) )
        block = block->next;

    // Reached the end
    if ( !block->available(size) )
    {
        std::size_t page_size = (size + durgos::page_size - 1) / durgos::page_size * durgos::page_size;
        block->next = (Block*)durgos::page_malloc(page_size);
        block = block->next;
        block->next = nullptr;
        block->used = false;
        block->size = page_size;
    }

    block->used = true;
    if ( block->size > size + sizeof(Block) )
        block->split(size);

    return block->memory();
}

void operator delete(void* ptr) noexcept
{
    Block* block = (Block*)ptr;
    --block;
    block->used = false;
    block->expand();

    Block* prev = &pool_head;
    while ( prev->next && prev->next != block )
        prev = prev->next;
    if ( prev->used == false )
        prev->expand();
}

void operator delete(void* ptr, const std::nothrow_t&) noexcept { return operator delete(ptr); }
void operator delete(void* ptr, std::size_t) noexcept { return operator delete(ptr); }
