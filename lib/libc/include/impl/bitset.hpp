#ifndef DURGOS_LIBCXX_BITSET
#define DURGOS_LIBCXX_BITSET

#include <cstdint>

namespace std {

namespace detail {
    // int_least<N>::type contains the smallest (unsigned) integer of size >= N bits
    template <size_t N> struct int_least
    {
        using type = typename int_least<N-1>::type;
        static_assert(sizeof(type) * 8 >= N, "Cannot find a large enough integer");
    };
    template<> struct int_least<33> {
        static_assert(sizeof(uintmax_t) * 8 > 32, "64 bit integers are not supported");
        using type = uintmax_t;
    };
    template<> struct int_least<17> { using type = uint32_t; };
    template<> struct int_least<9> { using type = uint16_t; };
    template<> struct int_least<0> { using type = uint8_t; };
    template <size_t N> using int_least_t = typename int_least<N>::type;

    // int_mask<N>::value contains a value with all first N bits to 1
    template <size_t N>
        struct int_mask
    {
        static constexpr int_least_t<N> value = (int_mask<N-1>::value << 1) | 1;
    };

    template <>
        struct int_mask<0>
    {
        static constexpr int_least_t<0> value = 0;
    };

} // namespace name

template<size_t N>
class bitset
{
private:
    using int_type = typename detail::int_least_t<N>;
    static constexpr int_type full_mask = detail::int_mask<N>::value;

public:
    class reference
    {
    public:
        constexpr reference& operator=(bool b) noexcept
        {
            if ( b )
                value |= mask;
            else
                value &= ~mask;
            return *this;
        }
        constexpr reference& operator=(const reference& x) noexcept = default;

        constexpr operator bool() const noexcept
        {
            return value & mask;
        }

        constexpr bool operator~() const noexcept
        {
            return !(value & mask);
        }

        constexpr reference& flip() noexcept
        {
            *this = ~*this;
        }

    private:
        constexpr reference(int_type& value, int_type offset) noexcept
        : value(value), mask(int_type(1) << offset) {}

        friend class bitset;
        int_type mask;
        int_type& value;
    };


    constexpr bitset(unsigned long long val = 0) noexcept
        : value(val) {}

    constexpr bool operator==(const bitset& oth) const noexcept
    {
        return value == oth.value;
    }

    constexpr bool operator!=(const bitset& oth) const noexcept
    {
        return value != oth.value;
    }

    constexpr bool operator[](std::size_t pos) const noexcept
    {
        return value & (int_type(1) << pos);
    }

    constexpr reference operator[](std::size_t pos) noexcept
    {
        return reference(value, pos);
    }

    // NOTE: it's supposed to throw out_of_range
    constexpr bool test(size_t pos) const noexcept
    {
        return (*this)[pos];
    }

    constexpr bool all() const noexcept
    {
        int_type mask = 1;
        for ( int i = 0; i < N; i++ )
        {
            if ( !(value & mask) )
                return false;
            mask <<= 1;
        }
        return true;
    }

    constexpr bool any() const noexcept
    {
        int_type mask = 1;
        for ( int i = 0; i < N; i++ )
        {
            if ( value & mask )
                return true;
            mask <<= 1;
        }
        return false;
    }

    constexpr bool none() const noexcept
    {
        int_type mask = 1;
        for ( int i = 0; i < N; i++ )
        {
            if ( value & mask )
                return false;
            mask <<= 1;
        }
        return true;
    }

    constexpr size_t count() const noexcept
    {
        size_t result = 0;
        int_type mask = 1;
        for ( int i = 0; i < N; i++ )
        {
            if ( value & mask )
                result++;
            mask <<= 1;
        }
        return result;
    }

    constexpr size_t size() const noexcept
    {
        return N;
    }

    constexpr bitset& operator&=(const bitset& other) noexcept
    {
        value &= other.value;
        return *this;
    }

    constexpr bitset& operator|=(const bitset& other) noexcept
    {
        value |= other.value;
        return *this;
    }

    constexpr bitset& operator^=(const bitset& other) noexcept
    {
        value ^= other.value;
        return *this;
    }

    constexpr bitset& operator<<=(size_t pos) noexcept
    {
        value <<= pos;
        return *this;
    }

    constexpr bitset& operator>>=(size_t pos) noexcept
    {
        value >>= pos;
        return *this;
    }

    constexpr bitset operator&(const bitset& other) const noexcept
    {
        return bitset(value & other.value);
    }

    constexpr bitset operator|=(const bitset& other) const noexcept
    {
        return bitset(value | other.value);
    }

    constexpr bitset operator^=(const bitset& other) const noexcept
    {
        return bitset(value ^ other.value);
    }

    constexpr bitset operator<<(size_t pos) const noexcept
    {
        return bitset(value << pos);
    }

    constexpr bitset operator>>(size_t pos) const noexcept
    {
        return bitset(value >> pos);
    }

    constexpr bitset operator~() const noexcept
    {
        return bitset(~value & full_mask);
    }

    constexpr bitset& set() noexcept
    {
        value = full_mask;
        return *this;
    }

    /// NOTE: it's supposed to throw out_of_range
    constexpr bitset& set(size_t pos, bool value = true) noexcept
    {
        (*this)[pos] = value;
        return *this;
    }

    constexpr bitset<N>& reset() noexcept
    {
        value = 0;
        return *this;
    }

    /// NOTE: it's supposed to throw out_of_range
    constexpr bitset& reset(size_t pos) noexcept
    {
        value &= ~(int_type(1) << pos);
    }

    constexpr bitset<N>& flip() noexcept
    {
        value = ~value | full_mask;
        return *this;
    }

    /// NOTE: it's supposed to throw out_of_range
    constexpr bitset<N>& flip(size_t pos) noexcept
    {
        (*this)[pos].flip();
        return *this;
    }

    /// NOTE: it's supposed to throw overflow_error
    constexpr unsigned long to_ulong() const noexcept
    {
        return value;
    }

    /// NOTE: it's supposed to throw overflow_error
    constexpr unsigned long long to_ullong() const noexcept
    {
        return value;
    }

private:
    int_type value;
};


} // namespace std

#endif // DURGOS_LIBCXX_BITSET
