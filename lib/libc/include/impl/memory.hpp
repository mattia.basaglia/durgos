#ifndef DURGOS_LIBCXX_MEMORY
#define DURGOS_LIBCXX_MEMORY

namespace std {

template< class T >
T* addressof(T& arg)
{
    return reinterpret_cast<T*>(
        &const_cast<char&>(
            reinterpret_cast<const volatile char&>(arg)
        )
    );
}

} // namespace std


#endif // DURGOS_LIBCXX_MEMORY
