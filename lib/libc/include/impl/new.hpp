#ifndef DURGOS_LIBCXX_NEW
#define DURGOS_LIBCXX_NEW

#include <cstddef>

namespace std {
  struct nothrow_t { explicit nothrow_t() = default; };
  extern const nothrow_t nothrow;
} // namespace std

// Exceptions are disabled so just implement this for now
void* operator new(std::size_t size, const std::nothrow_t&) noexcept;

void operator delete(void* ptr) noexcept;
void operator delete(void* ptr, const std::nothrow_t&) noexcept;
void operator delete(void* ptr, std::size_t) noexcept;

#endif // DURGOS_LIBCXX_NEW

