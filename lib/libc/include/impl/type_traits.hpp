#ifndef DURGOS_LIBCXX_TYPE_TRAITS
#define DURGOS_LIBCXX_TYPE_TRAITS

#include <cstddef>

namespace std {

template<class T, T val>
    struct integral_constant {
        static constexpr T value = val;
        using value_type = T;
        using type = integral_constant;
        constexpr operator value_type() const noexcept { return value; }
        constexpr value_type operator()() const noexcept { return value; }
    };
template<bool val>
    using bool_constant = integral_constant<bool, val>;
using false_type = bool_constant<false>;
using true_type = bool_constant<true>;

template<bool cond, class Ret=void> struct enable_if;
template<class Ret>                 struct enable_if<true, Ret> { using type = Ret; };
template<bool cond, class Ret=void> using enable_if_t = typename enable_if<cond, Ret>::type;

template<class T> struct remove_const          { using type = T; };
template<class T> struct remove_const<const T> { using type = T; };
template<class T> using remove_const_t = typename remove_const<T>::type;

template<class T> struct add_const          { using type = const T; };
template<class T> struct add_const<const T> { using type = const T; };
template<class T> using add_const_t = typename add_const<T>::type;

template<class T> struct remove_volatile             { using type = T; };
template<class T> struct remove_volatile<volatile T> { using type = T; };
template<class T> using remove_volatile_t = typename remove_volatile<T>::type;

template<class T> struct add_volatile          { using type = volatile T; };
template<class T> struct add_volatile<const T> { using type = volatile T; };
template<class T> using add_volatile_t = typename add_volatile<T>::type;

template<class T>
struct remove_cv {
    using type = std::remove_volatile_t<std::remove_const_t<T>>;
};
template<class T> using remove_cv_t = typename remove_cv<T>::type;

template<class T>
struct add_cv {
    using type = std::add_volatile_t<std::add_const_t<T>>;
};
template<class T> using add_cv_t = typename add_cv<T>::type;

template<class T> struct remove_reference       { using type = T; };
template<class T> struct remove_reference<T&>   { using type = T; };
template<class T> struct remove_reference<T&&>  { using type = T; };
template<class T> using remove_reference_t = typename remove_reference<T>::type;

template<bool B, class T, class F> struct conditional { typedef T type; };
template<class T, class F> struct conditional<false, T, F> { typedef F type; };
template<bool B, class T, class F> using conditional_t = typename conditional<B, T, F>::type;

template<class T> struct remove_extent { typedef T type; };
template<class T> struct remove_extent<T[]> { typedef T type; };
template<class T, size_t N> struct remove_extent<T[N]> { typedef T type; };
template<class T> using remove_extent_t = typename remove_extent<T>::type;

template<class T1, class T2> struct is_same : false_type {};
template<class T> struct is_same<T, T> : true_type {};
template<class T1, class T2> inline constexpr bool is_same_v = is_same<T1, T2>::value;

template<class T> struct is_void : std::is_same<void, typename std::remove_cv<T>::type> {};
template<class T> inline constexpr bool is_void_v = is_void<T>::value;

template<class T> struct is_null_pointer : is_same<nullptr_t, remove_cv_t<T>> {};
template<class T> inline constexpr bool is_null_pointer_v = is_null_pointer<T>::value;

template<class T> struct is_reference      : false_type {};
template<class T> struct is_reference<T&>  : true_type {};
template<class T> struct is_reference<T&&> : true_type {};
template<class T> inline constexpr bool is_reference_v = is_reference<T>::value;

namespace detail {
    template<class T> struct is_member_pointer: false_type {};
    template<class T, class U> struct is_member_pointer<T U::*> : true_type {};
} // namespace detail

template< class T > struct is_member_pointer: detail::is_member_pointer<remove_cv_t<T>> {};
template<class T> inline constexpr bool is_member_pointer_v = is_member_pointer<T>::value;

template<class T> struct is_array : false_type {};
template<class T> struct is_array<T[]> : true_type {};
template<class T, size_t N> struct is_array<T[N]> : true_type {};
template<class T> inline constexpr bool is_array_v = is_array<T>::value;

namespace detail {
    // base
    template<class T> struct remove_specifiers { using type = T; };
    // cv
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile> { using type = Ret(Args...); };
    // lref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile &> { using type = Ret(Args...); };
    // rref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile &&> { using type = Ret(Args...); };
    // noexcept plain
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile noexcept> { using type = Ret(Args...); };
    // noexcept lref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile & noexcept> { using type = Ret(Args...); };
    // noexcept rref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) volatile && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args...) const volatile && noexcept> { using type = Ret(Args...); };
    // varrags plain
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...)> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile> { using type = Ret(Args...); };
    // varargs lref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile &> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile &> { using type = Ret(Args...); };
    // varargs rref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile &&> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile &&> { using type = Ret(Args...); };
    // varrags noexcept plain
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile noexcept> { using type = Ret(Args...); };
    // varargs noexcept lref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile & noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile & noexcept> { using type = Ret(Args...); };
    // varargs noexcept rref
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) volatile && noexcept> { using type = Ret(Args...); };
    template<class Ret, class... Args>
        struct remove_specifiers<Ret(Args..., ...) const volatile && noexcept> { using type = Ret(Args...); };


    template<class>
        struct is_function : false_type { };
    template<class Ret, class... Args>
        struct is_function<Ret(Args...)> : true_type {};
    template<class Ret, class... Args>
        struct is_function<Ret(Args..., ...)> : true_type {};

    template<class T> struct add_pointer { using type = T; };
    template<class Ret, class... Args> struct add_pointer<Ret(Args...)> { using type = Ret(*)(Args...); };
    template<class Ret, class... Args> struct add_pointer<Ret(Args..., ...)> { using type = Ret(*)(Args..., ...); };


    template< class T >
        struct is_member_function_pointer : false_type {};
    template< class T, class U>
        struct is_member_function_pointer<T U::*>
            : is_function<typename detail::remove_specifiers<T>::type> {};

} // namespace detail

template<class T> struct is_function : detail::is_function<typename detail::remove_specifiers<T>::type>{};
template<class T> inline constexpr bool is_function_v = is_function<T>::value;

template<class T> struct add_pointer : detail::add_pointer<typename detail::remove_specifiers<T>::type>{};
template<class T> using add_pointer_t = typename add_pointer<T>::type;

template<class T>
struct decay
{
private:
    using U = remove_reference_t<T>;
public:
    using type = conditional_t<
        is_array_v<U>,
        remove_extent_t<U>*,
        conditional_t<
            is_function_v<U>,
            add_pointer_t<U>,
            remove_cv_t<U>
        >
    >;
};
template<class T> using decay_t = typename decay<T>::type;

template<class T>
    using is_member_function_pointer = detail::is_member_function_pointer<remove_cv_t<T>>;
template<class T> inline constexpr bool is_member_function_pointer_v = is_member_function_pointer<T>::value;

template<class...> using void_t = void;

template<typename T> struct is_object : bool_constant<!(is_function_v<T> || is_reference_v<T> || is_void_v<T>)> {};

namespace detail {
    template<typename T> struct can_be_referenced : bool_constant<!(is_function_v<T> || is_void_v<T>)> {};

    template<typename T, bool = can_be_referenced<T>::value>
        struct add_reference
        {
            using lvalue = T;
            using rvalue = T;
        };

    template<typename T>
        struct add_reference<T, true>
        {
            using lvalue = T&;
            using rvalue = T&&;
        };
} // namespace detail

template<typename T> struct add_rvalue_reference { using type = typename detail::add_reference<T>::rvalue; };
template<typename T> using add_rvalue_reference_t = typename add_rvalue_reference<T>::type;
template<typename T> struct add_lvalue_reference { using type = typename detail::add_reference<T>::lvalue; };
template<typename T> using add_lvalue_reference_t = typename add_lvalue_reference<T>::type;

template<class T> add_rvalue_reference_t<T> declval() noexcept;

// GCC builtins
template<typename T> struct is_class : public integral_constant<bool, __is_class(T)>{};
template <class T> inline constexpr bool is_class_v = is_class<T>::value;
template<typename T> struct is_union : public integral_constant<bool, __is_union(T)>{};
template <class T> inline constexpr bool is_union_v = is_union<T>::value;
template<typename T> struct is_enum : public integral_constant<bool, __is_enum(T)>{};
template <class T> inline constexpr bool is_enum_v = is_enum<T>::value;

namespace detail {
    template <class Base> true_type is_base_of_overload(const volatile Base*);
    template <class Base> false_type is_base_of_overload(const volatile void*);
    // well formed if Derived : Base (Base*) or Base and Derived aren't related (void*)
    template <class Base, class Derived>
        using is_base_of_ol_result = decltype(is_base_of_overload<Base>(declval<Derived*>()));

    // fall back if Base is not an accessible base of Derived
    template <class Base, class Derived, class = void>
    struct is_base_of : public std::true_type { };

    // Use overloading resolution to determine if a pointer to Derived can be converted to Base
    template <class Base, class Derived>
    struct is_base_of<Base, Derived, std::void_t<is_base_of_ol_result<Base, Derived>>> :
        public is_base_of_ol_result<Base, Derived> { };
} // namespace detail

template <class Base, class Derived>
struct is_base_of : std::conditional_t<
        std::is_class_v<Base> && std::is_class_v<Derived>,
        detail::is_base_of<Base, Derived>,
        std::false_type
    > { };
template <class Base, class Derived>
    inline constexpr bool is_base_of_v = is_base_of<Base, Derived>::value;


} // namespace std


#endif // DURGOS_LIBCXX_TYPE_TRAITS

