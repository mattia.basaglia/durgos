#ifndef LIBC_IMPL_CXX_H
#define LIBC_IMPL_CXX_H

#ifdef __cplusplus
#define LIBC_START_HEADER namespace std {
#define LIBC_END_HEADER }
#else
#define LIBC_START_HEADER
#define LIBC_END_HEADER
#endif

#endif // LIBC_IMPL_CXX_H
