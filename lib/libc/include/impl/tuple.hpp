#ifndef DURGOS_LIBCXX_TUPLE
#define DURGOS_LIBCXX_TUPLE

#include <utility>
#include <functional>
#include <type_traits>

namespace std {

namespace detail {
    template<size_t I, class H, class... T>
        struct get_tuple;

    struct ignore_t
    {
        template <class T>
            constexpr const ignore_t& operator=(const T&) const noexcept { return *this; }
    };
};

template<class... Tail>
class tuple;

template<class Head, class... Tail>
class tuple<Head, Tail...>
{
    using head_type = Head;
    using tail_type = tuple<Tail...>;

public:
    explicit constexpr tuple(Head&& head, Tail&&... tail)
        : value(forward<Head>(head)), tail(forward<Tail>(tail)...)
    {}

    constexpr tuple() = default;
    constexpr tuple(const tuple&) = default;
    constexpr tuple(tuple&&) = default;

    explicit constexpr tuple(const Head& head, const Tail&... tail)
        : value(head), tail(tail...)
    {}

    template<class HeadT, class... TailT>
    explicit constexpr tuple(HeadT&& head, TailT&&... tail)
        : value(std::forward<HeadT>(head)),
          tail(std::forward<TailT>(tail)...)
    {}

    template<class... U>
    constexpr tuple(tuple<U...>&& other)
        : value(std::move(other.value)),
          tail(std::move(other.tail))
    {}

    template<class... U>
    constexpr tuple(const tuple<U...>& other)
        : value(other.value),
          tail(other.tail)
    {}

    template<class U1, class U2>
        constexpr tuple(const pair<U1, U2>& p)
        : value(p.first), tail(p.second)
    {}

    template< class U1, class U2 >
        constexpr tuple(pair<U1, U2>&& p)
        : value(std::forward<U1>(p.first)),
          tail(std::forward<U2>(p.second))
    {}

    tuple& operator=(const tuple& other) = default;
    tuple& operator=(tuple&& other) = default;
    template<class... U>
        tuple& operator=(const tuple<U...>& other)
        {
            value = other.value;
            tail = other.tail;
        }
    template< class... U>
        tuple& operator=(tuple<U...>&& other)
        {
            swap(other);
        }

    void swap(tuple& other)
    {
        std::swap(value, other.value);
        std::swap(tail, other.tail);
    }

private:
    Head value;
    tail_type tail;

    template<class... T> friend class tuple;
    template<size_t I, class H, class... T> friend struct detail::get_tuple;
};


template<class Head>
class tuple<Head>
{
    using head_type = Head;
public:
    explicit constexpr tuple(head_type&& tail)
        : value(forward<head_type>(tail))
    {}
    explicit constexpr tuple(const head_type& head)
        : value(head)
    {}

    constexpr tuple() = default;
    constexpr tuple(const tuple&) = default;
    constexpr tuple(tuple&&) = default;


    template<class head_typeT>
    explicit constexpr tuple(head_typeT&& head)
        : value(std::forward<head_typeT>(head))
    {}

    template<class U>
    constexpr tuple(tuple<U>&& other)
        : value(std::move(other.value))
    {}

    template<class U>
    constexpr tuple(const tuple<U>& other)
        : value(other.value)
    {}

    tuple& operator=(const tuple& other) = default;
    tuple& operator=(tuple&& other) = default;

    template<class U>
        tuple& operator=(const tuple<U>& other)
        {
            value = other.value;
        }
    template<class U>
        tuple& operator=(tuple<U>&& other)
        {
            swap(other);
        }

    void swap(tuple& other)
    {
        std::swap(value, other.value);
    }

private:
    head_type value;

    template<class... T> friend class tuple;
    template<size_t I, class H, class... T> friend struct detail::get_tuple;
};



template<class Head>
class tuple<Head&>
{
    using head_type = Head&;
public:
    explicit constexpr tuple(head_type head)
        : value(head)
    {}

    constexpr tuple() = default;
    constexpr tuple(const tuple&) = default;
    constexpr tuple(tuple&&) = default;


    template<class head_typeT>
    explicit constexpr tuple(head_typeT&& head)
        : value(std::forward<head_typeT>(head))
    {}

    template<class U>
    constexpr tuple(tuple<U>&& other)
        : value(std::move(other.value))
    {}

    template<class U>
    constexpr tuple(const tuple<U>& other)
        : value(other.value)
    {}

    tuple& operator=(const tuple& other) = default;
    tuple& operator=(tuple&& other) = default;

    template<class U>
        tuple& operator=(const tuple<U>& other)
        {
            value = other.value;
        }
    template<class U>
        tuple& operator=(tuple<U>&& other)
        {
            swap(other);
        }

    void swap(tuple& other)
    {
        std::swap(value, other.value);
    }

private:
    head_type value;

    template<class... T> friend class tuple;
    template<size_t I, class H, class... T> friend struct detail::get_tuple;
};

template<size_t I, class T> struct tuple_element;
template<size_t I, class Head, class... Tail>
    struct tuple_element<I, std::tuple<Head, Tail...>>
        : std::tuple_element<I-1, std::tuple<Tail...>>
    {};
template<class Head, class... Tail>
    struct tuple_element<0, std::tuple<Head, Tail...>>
    {
        using type = Head;
    };

template<size_t I, class T>
    struct tuple_element< I, const T>
    {
        using type = std::add_const_t<typename tuple_element<I, T>::type>;
    };
template<size_t I, class T>
    struct tuple_element<I, volatile T>
    {
        using type = std::add_volatile_t<typename tuple_element<I, T>::type>;
    };
template<size_t I, class T>
    struct tuple_element< I, const volatile T >
    {
        using type = std::add_cv_t<typename tuple_element<I, T>::type>;
    };

template<size_t I, class T> using tuple_element_t = typename tuple_element<I, T>::type;

namespace detail {
    template<size_t I, class H, class... T>
        struct get_tuple
        {
            using recursive = get_tuple<I-1, T...>;
            using tuple_type = tuple<H, T...>;
            using tuple_tail_type = tuple<T...>;
            using out_type = typename recursive::out_type;
            static constexpr out_type& get(tuple_type& tup) noexcept
            {
                return recursive::get(tup.tail);
            }
            static constexpr const out_type& get(const tuple_type& tup) noexcept
            {
                return recursive::get(tup.tail);
            }
            static constexpr out_type&& get(tuple_type&& tup) noexcept
            {
                return recursive::get(std::move(tup.tail));
            }
        };

    template<class H, class... T>
        struct get_tuple<0, H, T...>
        {
            using tuple_type = tuple<H, T...>;
            using out_type = typename tuple_type::head_type;
            static constexpr out_type& get(tuple_type& tup) noexcept { return tup.value; }
            static constexpr const out_type& get(const tuple_type& tup) noexcept { return tup.value; }
            static constexpr out_type&& get(tuple_type&& tup) noexcept { return std::move(tup.value); }
        };

    template<class T> struct get_size;
    template<class... T>
        struct get_size<tuple<T...>>
        {
            static constexpr size_t size = sizeof...(T);
        };

} // namespace detail

template<size_t I, class... Types>
    constexpr auto& get(tuple<Types...>& t) noexcept
{
    return detail::get_tuple<I, Types...>::get(t);
}

template<size_t I, class... Types >
    constexpr auto&& get(tuple<Types...>&& t) noexcept
{
    return std::move(detail::get_tuple<I, Types...>::get(t));
}

template<size_t I, class... Types>
    constexpr const auto& get(const tuple<Types...>& t) noexcept
{
    return detail::get_tuple<I, Types...>::get(t);
}

template<class T> using tuple_size = integral_constant<size_t,
    detail::get_size<
        remove_cv_t<
            remove_reference_t<T>
        >
    >::size
>;
template<class T> inline constexpr size_t tuple_size_v = tuple_size<T>::value;


template <class... T>
constexpr auto make_tuple(T&&... args)
{
    return tuple<detail::decay_wrapper_t<T>...>(std::forward<T>(args)...);
}

inline constexpr detail::ignore_t ignore{};

template<class... Types>
    constexpr tuple<Types&...> tie(Types&... args) noexcept
    {
        return tuple<Types&...>(args...);
    }



} // namespace std
#endif // DURGOS_LIBCXX_TUPLE
