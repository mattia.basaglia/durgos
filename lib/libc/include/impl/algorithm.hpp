#ifndef DURGOS_LIBCXX_ALGORITHM
#define DURGOS_LIBCXX_ALGORITHM

#include <utility> // needs to define swap()

namespace std {

template<class T>
constexpr const T& min(const T& a, const T& b)
{
    return a <= b ? a : b;
}


template<class T>
constexpr const T& max(const T& a, const T& b)
{
    return a >= b ? a : b;
}


} // namespace std

#endif // DURGOS_LIBCXX_ALGORITHM

