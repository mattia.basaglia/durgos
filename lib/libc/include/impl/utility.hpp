#ifndef DURGOS_LIBCXX_UTILITY
#define DURGOS_LIBCXX_UTILITY

#include <type_traits>

namespace std {


// lvalue
template<class T>
    constexpr T&& forward(remove_reference_t<T>& t) noexcept
{
    return static_cast<T&&>(t);
}
// rvalue
template<class T>
    constexpr T&& forward(remove_reference_t<T>&& t) noexcept
{
      return static_cast<T&&>(t);
}

template<class T>
constexpr remove_reference_t<T>&& move(T&& t) noexcept
{
    static_cast<remove_reference_t<T>&&>(t);
}

template<class T>
    void swap(T& a, T& b)
    {
        T c = move(a);
        a = move(b);
        b = move(c);
    }

struct piecewise_construct_t
{
    explicit piecewise_construct_t() = default;
};

inline constexpr piecewise_construct_t piecewise_construct{};

template<class...> class tuple;


template<class T1, class T2>
    class pair
{
public:
    using first_type = T1;
    using second_type = T2;

    constexpr pair() = default;
    constexpr pair(const T1& first, const T2& second)
        : first(first), second(second)
    {}
    template<class U1, class U2>
        constexpr pair(U1&& x, U2&& y)
        : first(forward<U1>(x)), second(forward<U2>(y))
    {}
    template<class U1, class U2>
        constexpr pair(const pair<U1, U2>& p)
        : first(p.first), second(p.second)
    {}
    template<class U1, class U2>
        constexpr pair(pair<U1, U2>&& p)
        : first(forward<U1>(p.first)), second(forward<U2>(p.second))
    {}
    // TODO defined in tuple.hpp
    template<class... Args1, class... Args2>
    constexpr pair(
        piecewise_construct_t,
        tuple<Args1...> first_args,
        tuple<Args2...> second_args
    );

    pair& operator=(const pair& other) = default;
    template<class U1, class U2>
        pair& operator=(const pair<U1,U2>& other)
    {
        first = other.first;
        second = other.second;
    }
    pair& operator=(pair&& other) noexcept // TODO is_nothrow_move_assignable
    {
        swap(other);
    }
    template<class U1, class U2>
        pair& operator=(pair<U1,U2>&& other)
    {
        first = move(other.first);
        second = move(other.second);
    }
    void swap(pair& other) noexcept // TODO is_nothrow_swappable
    {
        swap(first, other.first);
        swap(second, other.second);
    }

    first_type first;
    second_type second;
};

} // namespace std
#endif // DURGOS_LIBCXX_UTILITY
