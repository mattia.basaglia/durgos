#ifndef DURGOS_LIBCXX_FUNCTIONAL
#define DURGOS_LIBCXX_FUNCTIONAL

#include <type_traits>
#include <memory>
#include "impl/utility.hpp"

namespace std {


namespace detail {
    template <class T> struct is_reference_wrapper : false_type {};
    template <class T>
        constexpr bool is_reference_wrapper_v = is_reference_wrapper<T>::value;

    template <class T, class Type, class T1, class... Args>
        decltype(auto) invoke(Type T::* f, T1&& t1, Args&&... args)
    {
        if constexpr (is_member_function_pointer_v<decltype(f)>)
        {
            if constexpr ( is_base_of_v<T, decay_t<T1>> )
                return (forward<T1>(t1).*f)(forward<Args>(args)...);
            else if constexpr ( is_reference_wrapper_v<decay_t<T1>> )
                return (t1.get().*f)(forward<Args>(args)...);
            else
                return ((*forward<T1>(t1)).*f)(forward<Args>(args)...);
        }
        else
        {
            if constexpr ( is_base_of_v<T, decay_t<T1>> )
                return forward<T1>(t1).*f;
            else if constexpr ( is_reference_wrapper_v<decay_t<T1>> )
                return t1.get().*f;
            else
                return (*forward<T1>(t1)).*f;
        }
    }

    template <class F, class... Args>
        decltype(auto) invoke(F&& f, Args&&... args)
    {
        return forward<F>(f)(forward<Args>(args)...);
    }
} // namespace detail

template< class F, class... Args>
decltype(auto) invoke(F&& f, Args&&... args) // TODO nothrow() requirements
{
    return detail::invoke(forward<F>(f), forward<Args>(args)...);
}


template <class T>
    class reference_wrapper
{
public:
    using type = T;

    reference_wrapper(T& ref) noexcept : wrapped(addressof(ref)) {}
    reference_wrapper(T&&) = delete;
    reference_wrapper(const reference_wrapper&) noexcept = default;
    reference_wrapper& operator=(const reference_wrapper& x) noexcept = default;

    operator T& () const noexcept { return *wrapped; }
    T& get() const noexcept { return *wrapped; }

    template< class... ArgTypes >
        decltype(auto) operator() ( ArgTypes&&... args ) const
    {
        return invoke(get(), forward<ArgTypes>(args)...);
    }

private:
    T* wrapped;
};

namespace detail {

template <class T>
    struct is_reference_wrapper<reference_wrapper<T>> : std::true_type {};

    template <class T>
    struct decay_wrapper
    {
        using type = T;
    };

    template <class T>
    struct decay_wrapper<reference_wrapper<T>>
    {
        using type = T&;
    };

    // Can't just use conditional because we would need to get reference_wrapper::type&
    // which fails for other
    template <class T>
    using decay_wrapper_t = typename decay_wrapper<decay_t<T>>::type;

} // namespace detail


template<class T1, class T2>
    constexpr auto make_pair(T1&& first, T2&& second)
{
    using V1 = detail::decay_wrapper_t<T1>;
    using V2 = detail::decay_wrapper_t<T2>;
    return pair<V1, V2>(forward<T1>(first), forward<T1>(second));
}


} // namespace std

#endif // DURGOS_LIBCXX_FUNCTIONAL
