#ifndef LIBC_STDLIB
#define LIBC_STDLIB

#include "impl/cxx.h"
#include <stddef.h>
#include <stdint.h>

LIBC_START_HEADER


void *malloc(size_t size);
void free(void *ptr);

LIBC_END_HEADER
#endif // LIBC_STDLIB


