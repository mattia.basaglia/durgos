#ifndef LIBC_STDDEF
#define LIBC_STDDEF

#include "impl/cxx.h"

LIBC_START_HEADER


typedef unsigned long size_t;

#ifdef __cplusplus
    using nullptr_t = decltype(nullptr);
#endif

LIBC_END_HEADER
#endif // LIBC_STDDEF
