#ifndef LIBC_STRING_H
#define LIBC_STRING_H


#include "impl/cxx.h"
#include "stddef.h"

LIBC_START_HEADER


size_t strlen(const char* str);
int strcmp(const char *lhs, const char *rhs);
int strncmp(const char* lhs, const char* rhs, size_t count);
void* memset(void *s, int c, size_t n);
void* memcpy(void *dest, const void *src, size_t n);
void* memmove(void *dest, const void *src, size_t n);
int memcmp(const void* s1, const void* s2, size_t n);

LIBC_END_HEADER

#endif // LIBC_STRING_H
