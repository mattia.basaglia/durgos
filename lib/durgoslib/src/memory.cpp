#include <durgos/memory.hpp>


namespace durgos {
/* This should be computed at link time, but a hardcoded
 * value is fine for now. Remember that our kernel starts
 * at 0x1000 as defined on the Makefile */
static std::uintptr_t free_mem_addr = 0x10000;

// Implementation is just a pointer to some free memory which keeps growing
std::uintptr_t page_malloc(std::size_t size)
{
    // Pages are aligned to 4K, or 0x1000
    if ( free_mem_addr & 0xFFFFF000 )
    {
        free_mem_addr &= 0xFFFFF000;
        free_mem_addr += page_size;
    }

    std::uintptr_t ret = free_mem_addr;
    free_mem_addr += size;
    return ret;
}

} // namespace durgos
