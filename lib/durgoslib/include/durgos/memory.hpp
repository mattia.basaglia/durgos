#ifndef DURGOSLIB_MEMORY_HPP
#define DURGOSLIB_MEMORY_HPP
#include <cstdint>
#include <cstddef>

namespace durgos {
    static const std::size_t page_size = 0x1000;
    std::uintptr_t page_malloc(std::size_t size);
} // namespace durgos

#endif // DURGOSLIB_MEMORY_HPP
