#ifndef DURGOSLIB_STRING
#define DURGOSLIB_STRING

#include <cstddef>

namespace durgos {

template<class T>
constexpr void pod_copy(const T *source, T *dest, int count) noexcept
{
    for (; count >= 0; --count) {
        *(dest++) = *(source++);
    }
}

namespace detail {
    template<int base, class T>
        inline constexpr char last_digit(T& value) noexcept
        {
            auto rem = value % base;
            value /= base;
            return rem > 9 ? rem + 'A' - 10 : rem + '0';
        }
} // detail

template<int base=10, class T>
constexpr void int_to_str(T val, char *out, unsigned size) noexcept
{
    if ( size <= 0 )
        return;

    if ( val < 0 )
    {
        *out++ = '-';
        val = -val;
        size--;
    }

    out[size-1] = '\0';
    out = out + size - 2;

    while ( --size )
    {
        *out-- = detail::last_digit<base>(val);
    }

}

template<int base=10, class T, int S>
constexpr void int_to_str(T val, char (&out)[S]) noexcept
{
    int_to_str<base>(val, out, S);
}


inline constexpr int strcmp(const char* lhs, const char* rhs) noexcept
{
    int i = 0;
    for (; lhs[i] == rhs[i]; i++)
    {
        if (lhs[i] == '\0')
            return 0;
    }
    return lhs[i] - rhs[i];
}

inline constexpr int strncmp(const char* lhs, const char* rhs, std::size_t count) noexcept
{
    int i = 0;
    for (; count && lhs[i] == rhs[i]; i++)
    {
        if (lhs[i] == '\0')
            return 0;
        count--;
    }
    if ( count == 0 )
        return 0;
    return lhs[i] - rhs[i];
}

} // namespace durgos
#endif // DURGOSLIB_STRING
