#ifndef DURGOSLIB_MANAGED_STRING_HPP
#define DURGOSLIB_MANAGED_STRING_HPP

#include <cstdint>
#include <string_view>
#include <algorithm>

namespace durgos {

using ROString = std::basic_string_view<const char>;

template<std::size_t Size>
class CheckedBuffer
{
public:
    static constexpr std::size_t max_size = Size;
    using value_type = char;
    using iterator = char*;
    using const_iterator = const char*;

    constexpr char& operator[](std::size_t index) noexcept
    {
        return buffer[index];
    }

    constexpr std::size_t size() const noexcept
    {
        return size_index;
    }

    constexpr void clear() noexcept
    {
        size_index = 0;
        buffer[size_index] = '\0';
    }

    constexpr void push_back(char c) noexcept
    {
        if ( size_index < max_size )
        {
            buffer[size_index] = c;
            size_index++;
            buffer[size_index] = '\0';
        }
    }

    constexpr void pop_back() noexcept
    {
        if ( size_index > 0 )
        {
            buffer[size_index] = '\0';
            size_index--;
        }
    }

    constexpr char* data() noexcept
    {
        return buffer;
    }

    constexpr CheckedBuffer clone() noexcept
    {
        CheckedBuffer buf;
        buf.size_index = size_index;
        pod_copy(buffer, buf.buffer, size_index);
        return buf;
    }

    constexpr ROString ro_view() const noexcept
    {
        return ROString(buffer, size_index);
    }

    constexpr iterator begin() noexcept { return buffer; }
    constexpr const_iterator begin() const noexcept { return buffer; }
    constexpr const_iterator cbegin() const noexcept { return buffer; }
    constexpr iterator end() noexcept { return buffer + size_index; }
    constexpr const_iterator end() const noexcept { return buffer + size_index; }
    constexpr const_iterator cend() const noexcept { return buffer + size_index; }

    constexpr void resize(std::size_t sz) noexcept
    {
        size_index = std::min(sz, max_size);
    }

private:
    std::size_t size_index = 0;
    char buffer[max_size];
    const char nulcheck = '\0';
};



template<class CharT>
class StringIterationSize
{
public:
    using value_type = CharT;
    using pointer = CharT*;
    using size_type = std::size_t;

    explicit constexpr StringIterationSize(size_type size) noexcept : sz(size) {}

    constexpr bool is_end(pointer,  std::size_t offset) const noexcept
    {
        return offset >= sz;
    }

    constexpr bool operator==(const StringIterationSize& oth) const noexcept
    {
        return sz == oth.sz;
    }

private:
    size_type sz;
};

template<class CharT>
class StringIterationNul
{
public:
    using value_type = CharT;
    using pointer = CharT*;
    using size_type = std::size_t;

    constexpr bool is_end(pointer start,  std::size_t offset) const noexcept
    {
        return start[offset] == '\0';
    }

    constexpr bool operator==(const StringIterationNul&) const noexcept
    {
        return true;
    }
};


template<class IterationPolicy>
class TokenIterator
{
public:
    using value_type = std::basic_string_view<typename IterationPolicy::value_type>;
    using size_type = typename IterationPolicy::size_type;
    using pointer = typename IterationPolicy::pointer;
    using iterator = TokenIterator;

    constexpr TokenIterator(
        IterationPolicy iteration,
        pointer string,
        char separator = ' ',
        bool skip_empty = true
    ) noexcept
     : iteration(iteration),
        base(string),
        separator(separator),
        skip_empty(skip_empty)
    {
        advance();
    }

    constexpr void reset() const
    {
        offset = next_offset = 0;
        advance();
    }

    constexpr TokenIterator begin() const noexcept
    {
        return *this;
    }

    constexpr TokenIterator end() const noexcept
    {
        return TokenIterator(iteration, nullptr, separator, skip_empty);
    }

    constexpr ROString operator*() const noexcept
    {
        return ROString(base + offset, base + next_offset);
    }

    constexpr TokenIterator& operator++() noexcept
    {
        advance();
        return *this;
    }

    constexpr TokenIterator operator++(int) noexcept
    {
        auto copy = *this;
        advance();
        return copy;
    }

    constexpr bool operator==(const TokenIterator& other) const noexcept
    {
        if ( is_end() && other.is_end() )
            return true;

        return
            iteration == other.iteration &&
            base == other.base &&
            offset == other.offset &&
            next_offset == other.next_offset &&
            separator == other.separator
            // Checking skip_empty probably uncesessary as they point to the same string anyway
        ;
    }

    constexpr bool operator!=(const TokenIterator& other) const noexcept
    {
        return !(*this == other);
    }

private:
    constexpr bool is_end() const noexcept
    {
        return !base || iteration.is_end(base, offset);
    }

    constexpr void advance() noexcept
    {
        if ( !base )
            return;

        while ( iteration.is_end(base, next_offset) )
        {
            offset = next_offset;
            return;
        }

        do
        {
            if ( next_offset != 0 )
                next_offset++;
            offset = next_offset;
            while ( !iteration.is_end(base, next_offset) && base[next_offset] != separator )
            {
                next_offset++;
            }
        } while ( skip_empty && offset == next_offset && !iteration.is_end(base, next_offset) );
    }

    IterationPolicy iteration;
    pointer base;
    size_type offset = 0;
    size_type next_offset = 0;
    char separator;
    bool skip_empty;
};

using ROStringTokenizer = TokenIterator<StringIterationSize<const char>>;
inline constexpr ROStringTokenizer token_iterator(
    const ROString& str,
    char separator = ' ',
    bool skip_empty = true
) noexcept
{
    return ROStringTokenizer(
        StringIterationSize<const char>(str.size()),
        str.data(),
        separator,
        skip_empty
    );
}

} // namespace durgos
#endif // DURGOSLIB_MANAGED_STRING_HPP
