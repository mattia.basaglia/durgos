DurgOS
======


Dependencies
------------

Needs a gcc cross compiler, if built from `make compiler` it needs the gcc
dependencies, most notably:
* libgmp
* libmpfr
* libmpc

Then to build the sources
* nasm

And to run the image
* qemu (qemu-system-x86_64)
