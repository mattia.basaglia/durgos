#include "callbacks.hpp"

#include <cstring>
#include <cstdlib>

#include "tty.hpp"
#include "drivers/vga_text.hpp"
#include <durgos/string.hpp>
#include "cpu/control.hpp"
#include "durg_animation.hpp"
#include "shell.hpp"

static std::uint32_t tick = 0;
DurgAnimation durg_anim;

void timer_callback()
{
    tick++;
    tty::Writer w;
    w.move_to(tty::driver.screen_columns / tty::driver.character_width - 40, 0);
    w << "Tick: " << tty::Number<10>(tick);
    durg_anim.draw(tick);
}

void user_input(durgos::ROString text)
{
    Shell::command(text);
}

void exception_callback(const cpu::interrupt::CallbackArgument& r, durgos::ROString name)
{
    tty::Writer w;
    w.move_to_cursor();
    w << "Exception " << tty::Number<2>(r.int_no) << ": " << name << '\n';
    w.align_cursor();
    shutdown();
}

void shutdown()
{
    tty::Writer w;
    w.move_to_cursor();
    w << "\n\nShutting down\nBye bye!\n";
    w.align_cursor();
    cpu::halt();
}

void welcome()
{
    tty::Writer w;
    w.fill_screen();
    w << "Welcome to " << tty::Style(tty::FG_BLUE|tty::FG_BRIGHT) << "DurgOS" << tty::Style(tty::FG_WHITE) << "!\n";
    w.align_cursor();
    Shell::command("");
}
