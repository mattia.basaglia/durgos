#ifndef KERNEL_TTY_HPP
#define KERNEL_TTY_HPP

#include <cstddef>
#include <cstdint>
#include <durgos/managed_string.hpp>

namespace tty {


struct Coord
{
    std::size_t x = 0;
    std::size_t y = 0;

    std::size_t to_offset() const;

    static Coord from_offset(std::size_t offset);
};

enum StyleEnum
{
    BG_BLACK            = 0x00,
    BG_BLUE             = 0x10,
    BG_GREEN            = 0x20,
    BG_CYAN             = 0x30,
    BG_RED              = 0x40,
    BG_MAGENTA          = 0x50,
    BG_YELLOW           = 0x60,
    BG_WHITE            = 0x70,
    BG_BRIGHT           = 0x80,

    FG_BLACK            = 0x00,
    FG_BLUE             = 0x01,
    FG_GREEN            = 0x02,
    FG_CYAN             = 0x03,
    FG_RED              = 0x04,
    FG_MAGENTA          = 0x05,
    FG_YELLOW           = 0x06,
    FG_WHITE            = 0x07,
    FG_BRIGHT           = 0x08,
};

struct Style
{
    explicit constexpr Style(std::uint8_t color_flags)
        : foreground(color_flags & 0xf),
            background((color_flags >> 4) & 0xf)
    {}

    constexpr Style(std::uint8_t foreground, std::uint8_t background)
        : foreground(foreground),
            background(background)
    {}

    constexpr std::uint8_t to_byte() const
    {
        return foreground | (background << 4);
    }

    std::uint8_t foreground;
    std::uint8_t background;
};

struct TtyDriver
{
    std::size_t character_width;
    std::size_t character_height;
    std::size_t screen_rows;
    std::size_t screen_columns;
    void (*set_cursor)(std::size_t offset);
    std::size_t (*get_cursor)();
    void (*fill_screen)(Style);
    void (*copy_block)(std::size_t offet_from, std::size_t offet_to, std::size_t count);
    void (*write_char)(std::size_t offet, char c, Style style);
};


template<std::size_t Digits, std::size_t Base=10>
class Number: public durgos::CheckedBuffer<Digits+1>
{
public:
    constexpr Number() noexcept = default;

    template<class N>
        constexpr explicit Number(N n) noexcept
    {
        durgos::int_to_str<Base>(n, this->data(), Number::max_size);
        this->resize(Number::max_size);
    }

    template<class N>
        constexpr Number& operator()(N n) noexcept
    {
        durgos::int_to_str<Base>(n, this->data(), Number::max_size);
        this->resize(Number::max_size);
        return *this;
    }
};


extern TtyDriver driver;

class Writer
{
public:

    Writer(Style current_style = Style(FG_WHITE), Coord pos = {})
    : current_style(current_style), pos(pos) {}

    Writer& operator<<(char c)
    {
        put(c);
        return *this;
    }

    Writer& operator<<(durgos::ROString str)
    {
        for ( char c : str )
            put(c);
        return *this;
    }

    template<std::size_t Size>
        Writer& operator<<(const durgos::CheckedBuffer<Size>& str)
    {
        for ( char c : str )
            put(c);
        return *this;
    }

    Writer& operator<<(Style s)
    {
        current_style = s;
        return *this;
    }

    void fill_screen()
    {
        driver.fill_screen(current_style);
        pos = {0, 0};
    }

    void move_to(unsigned short x, unsigned short y)
    {
        pos = Coord{x, y};
        if ( y * driver.character_height >= driver.screen_rows )
            scroll(y * driver.character_height - driver.screen_rows);
    }

    void move_to_cursor()
    {
        pos = Coord::from_offset(driver.get_cursor());
    }

    void align_cursor()
    {
        driver.set_cursor(pos.to_offset());
    }

    void scroll(int nlines=1)
    {
        if ( nlines < 0 )
            return;

        if (std::size_t(nlines) > driver.screen_rows )
        {
            fill_screen();
            pos.y = 0;
            return;
        }

        std::size_t count = (driver.screen_rows - nlines + 1) * driver.screen_columns -1;
        driver.copy_block(nlines * driver.screen_columns, 0, count);

        for ( std::size_t i = count; i <= driver.screen_rows * driver.screen_columns; ++i )
            driver.write_char(i, ' ', current_style);

        pos.y -= nlines;
    }

    void put(char c)
    {
        if ( c == '\n' )
            pos.y++, pos.x = 0;
        else if ( c == '\b' )
            pos.x--;
        else if ( c == '\r' )
            pos.x = 0;
        else if ( c == '\v' )
            pos.y++;
        else
        {
            driver.write_char(pos.to_offset(), c, current_style);
            pos.x++;
        }

        check_scroll();
    }

private:
    void check_scroll()
    {
        if ( pos.x >= driver.screen_columns / driver.character_width )
        {
            pos.x = 0;
            pos.y++;
        }

        if ( pos.y >= driver.screen_columns / driver.character_width )
            scroll();
    }

    Style current_style;
    Coord pos;
};

} // namespace tty


#endif // KERNEL_TTY_HPP
