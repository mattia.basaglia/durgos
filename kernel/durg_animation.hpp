#ifndef KERNEL_DURG_ANIMATION_HPP
#define KERNEL_DURG_ANIMATION_HPP


class DurgAnimation
{
public:
    void draw(std::uint32_t tick)
    {
        if ( !durg_on )
            return;

        int right_margin = tty::driver.screen_columns / tty::driver.character_width;
        int durg_x = right_margin - durg_width;

        tty::Writer w;
        for ( int y = 0; y < durg_height; y++ )
        {
            for ( int x = 0; x < durg_width; x++ )
            {
                int x_pos = durg_x + x;
                if ( durg_move )
                    x_pos += durg_off;
                w.move_to(x_pos % right_margin, durg_y + y);
                w << ' ';
            }
        }

        for ( int ci = 0; ci < durg_colors; ci++ )
        {
            w << tty::Style(durg_color_map[ci]);
            for ( int y = 0; y < durg_height; y++ )
            {
                for ( int x = 0; x < durg_width; x++ )
                {
                    char c = dragon_frames[ci][tick / 10 % durg_nframes][y][x];
                    if ( c != ' ' )
                    {
                        int x_pos = durg_x + x;
                        if ( durg_move )
                            x_pos += durg_off;
                        w.move_to(x_pos % right_margin, durg_y + y);
                        w << c;
                    }
                }
            }
        }
        if ( durg_move )
        {
            durg_off -= 1;
            if ( durg_off < 0 )
                durg_off += right_margin;
        }
    }

    void toggle(bool on)
    {
        durg_on = on;
    }

    void toggle_move()
    {
        durg_move = !durg_move;
    }

private:
    bool durg_on = false;
    bool durg_move = false;
    int durg_off = 0;
    static constexpr int durg_nframes = 2;
    static constexpr int durg_height = 13;
    static constexpr int durg_width = 34;
    static constexpr int durg_y = 2;
    static constexpr int durg_x = -durg_width;
    static constexpr int durg_colors = 5;
    static constexpr int durg_color_map[durg_colors] = {
        tty::FG_BLUE|tty::FG_BRIGHT,
        tty::FG_RED|tty::FG_BRIGHT,
        tty::FG_CYAN|tty::FG_BRIGHT,
        tty::FG_BLUE,
        tty::FG_WHITE,
    };
    static constexpr char dragon_frames[durg_colors][durg_nframes][durg_height][durg_width] = {
    {
        {
            R"(               ___               )",
            R"(              /   /`             )",
            R"(            // ..-_\             )",
            R"(          ,((:'    /`            )",
            R"(            \\''._\          ___ )",
            R"(   __//      \\   /`  _____/.-- >)",
            R"( --o  -----.^//    \^^  ---'     )",
            R"(`----^----'( ._____( /--         )",
            R"(            \\_,____\\/__,       )",
            R"(             \-/     \---/       )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        },
        {
            R"(                                 )",
            R"(                                 )",
            R"(                             ___ )",
            R"(   __//               _____/.-- >)",
            R"( --o  -----.^^^^^^^^^^  ---'     )",
            R"(`----^----'( \\   /( /--         )",
            R"(            \// _\,_\\/__,       )",
            R"(            //,' /   \---/       )",
            R"(          `((:,  _\,             )",
            R"(            \\ '' /              )",
            R"(              \___\              )",
            R"(                   `             )",
        }
    },
    {
        {
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(   o                             )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        },
        {
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(   o                             )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        }
    },
    {
        {
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                            .--  )",
            R"(                        ---'     )",
            R"( ----^----'   _____   --         )",
            R"(              _ ____   __        )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        },
        {
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                            .--  )",
            R"(                        ---'     )",
            R"( ----^----'           --         )",
            R"(                   _   __        )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        }
    },
    {
        {
            R"(                                 )",
            R"(                  /              )",
            R"(                   \             )",
            R"(                   /             )",
            R"(                  \          ___ )",
            R"(    _             /   _____/     )",
            R"(      -----.^      \^^           )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        },
        {
            R"(                                 )",
            R"(                                 )",
            R"(                             ___ )",
            R"(    _                 _____/     )",
            R"(      -----.^^^^^^^^^^           )",
            R"(                  /              )",
            R"(                 \               )",
            R"(                 /               )",
            R"(                  \              )",
            R"(                  /              )",
            R"(                  \              )",
            R"(                                 )",
        }
    },
    {
        {
            R"(                                 )",
            R"(                   `             )",
            R"(                                 )",
            R"(          ,         `            )",
            R"(                                 )",
            R"(     //            `             )",
            R"(                                 )",
            R"(                                 )",
            R"(               ,         ,       )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
        },
        {
            R"(                                 )",
            R"(                                 )",
            R"(                                 )",
            R"(     //                          )",
            R"(                                 )",
            R"(                                 )",
            R"(                  ,      ,       )",
            R"(                                 )",
            R"(          `        ,             )",
            R"(                                 )",
            R"(                                 )",
            R"(                   `             )",
        }
    },
    };
};

extern DurgAnimation durg_anim;

#endif // KERNEL_DURG_ANIMATION_HPP
