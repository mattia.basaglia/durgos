#ifndef KERNEL_CALLBACKS_HPP
#define KERNEL_CALLBACKS_HPP


#include "cpu/interrupts.hpp"
#include "drivers/keyboard.hpp"
#include <durgos/managed_string.hpp>

void shutdown();
void timer_callback();
void exception_callback(const cpu::interrupt::CallbackArgument& r, durgos::ROString name);
void user_input(durgos::ROString text);
void keyboard_callback(keyboard::KeyCode code, keyboard::EventType type);
void welcome();


#endif // KERNEL_CALLBACKS_HPP
