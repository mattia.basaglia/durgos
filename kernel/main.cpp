#include "cpu/interrupts.hpp"
#include "cpu/timer.hpp"
#include "callbacks.hpp"
#include "tty.hpp"
#include "drivers/vga_text.hpp"

void main()
{
    tty::driver = vga::TextMode::driver();
    cpu::interrupt::initialize();
    cpu::interrupt::register_exception_handler(exception_callback);
    cpu::timer::register_callback(50, timer_callback);
    keyboard::register_callback(keyboard_callback);
    welcome();
}
