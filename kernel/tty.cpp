#include "tty.hpp"

namespace tty {

TtyDriver driver;


std::size_t Coord::to_offset() const
{
    return x * driver.character_width + y * driver.screen_columns * driver.character_height;
}

Coord Coord::from_offset(std::size_t offset)
{
    return {
        (offset % driver.screen_columns) / driver.character_width,
        (offset / driver.screen_columns) / driver.character_height
    };
}


} // namespace tty

