#ifndef KERNEL_SHELL_HPP
#define KERNEL_SHELL_HPP

#include <tuple>
#include <cstdlib>
#include <durgos/managed_string.hpp>
#include "callbacks.hpp"
#include "tty.hpp"
#include "durg_animation.hpp"

template<class Callable>
class ShellCommand
{
public:
    constexpr ShellCommand(durgos::ROString name, Callable function)
        : name(name), function(function)
    {}

    bool run(durgos::ROString cmd, durgos::ROStringTokenizer tokens) const
    {
        if ( cmd != name )
            return false;
        function(tokens);
        return true;
    }

private:
    durgos::ROString name;
    Callable function;
};

template<class Callable>
    constexpr ShellCommand<Callable> shell_command(durgos::ROString name, Callable function)
    {
        return {name, function};
    }

template<int I, class T> struct ShellInvoker;

template<int I, class... T>
    struct ShellInvoker<I, std::tuple<T...>>
    {
        static bool invoke_shell(const std::tuple<T...>& tup, durgos::ROString cmd, durgos::ROStringTokenizer tokens)
        {
            if ( ShellInvoker<I-1, std::tuple<T...>>::invoke_shell(tup, cmd, tokens) )
                return true;
            return std::get<I>(tup).run(cmd, tokens);
        }
    };

template<class... T>
    struct ShellInvoker<0, std::tuple<T...>>
    {
        static bool invoke_shell(const std::tuple<T...>& tup, durgos::ROString cmd, durgos::ROStringTokenizer tokens)
        {
            return std::get<0>(tup).run(cmd, tokens);
        }
    };

// template<class... T>
class Shell
{
public:
    static void command(durgos::ROString text)
    {
        if ( !text.empty() )
        {
            auto tok = durgos::token_iterator(text);
            auto cmd = *tok;
            ++tok;
            if ( !ShellInvoker<std::tuple_size_v<cmds_type> - 1, cmds_type>::invoke_shell(cmds, cmd, tok) )
            {
                tty::Writer w;
                w.move_to_cursor();
                w << "Unknown command: " << text << "\n";
                w.align_cursor();
            }
        }
        tty::Writer w;
        w.move_to_cursor();
        w << "$ ";
        w.align_cursor();
    }

private:
    inline static auto cmds = std::make_tuple(
        shell_command("quit", [](durgos::ROStringTokenizer){ shutdown(); }),
        shell_command("malloc", [](durgos::ROStringTokenizer){
            tty::Writer w;
            w.move_to_cursor();
            void* ptr = std::malloc(128);
            w << "Allocated: " << tty::Number<16, 16>((std::uintptr_t)ptr) << "\n";
            w.align_cursor();
        }),
        shell_command("durg", [](durgos::ROStringTokenizer toks){
            auto cmd = *toks;
            if ( cmd == "on" )
                durg_anim.toggle(true);
            else if ( cmd == "off" )
                durg_anim.toggle(false);
            else if ( cmd == "move" )
                durg_anim.toggle_move();

        }),
        shell_command("clear", [](durgos::ROStringTokenizer){
            tty::Writer w;
            w.fill_screen();
            w.align_cursor();
        }),
        shell_command("echo", [](durgos::ROStringTokenizer tok){
            tty::Writer w;
            w.move_to_cursor();
            for ( auto str : tok )
                w << str << ' ';
            w << '\n';
            w.align_cursor();
        })
    );
    using cmds_type = std::decay_t<decltype(cmds)>;
};


#endif // KERNEL_SHELL_HPP
