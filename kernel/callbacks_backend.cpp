#include "callbacks.hpp"
#include "tty.hpp"
#include <durgos/string.hpp>
#include "shell.hpp"

enum KeyboardMode
{
    Default = 0x00,
    NoEcho = 0x01,
    Unbuffered = 0x02,
};

durgos::CheckedBuffer<256> key_buffer;
KeyboardMode key_mode = Default;

void keyboard_callback(keyboard::KeyCode code, keyboard::EventType type)
{
    tty::Number<4, 16> sc_ascii;
    tty::Writer w;
    w.move_to(tty::driver.screen_columns / tty::driver.character_width - 40, 1);
    w << "Key: 0x" << sc_ascii(code.scancode);

    if ( type == keyboard::EventType::Up )
        w << " U ";
    else
        w << " D ";

    if ( code.is_unknown() )
        w << "Unknown ";
    else if ( code.is_error() )
        w << "Error   ";
    else if ( code.code == (keyboard::Enter|keyboard::Keypad) )
        w << "KP Enter";
    else if ( code.is_keypad() )
        w << "Keypad " << char(code.code & 0xff);
    else if ( code.code == keyboard::Enter )
        w << "Enter   ";
    else if ( code.is_ascii() )
        w << "       " << char(code.code);
    else if ( code.code >= keyboard::F1 && code.code < keyboard::F10 )
        w << "F" << char((code.code - keyboard::F1) + '1') << "      ";
    else if ( code.code >= keyboard::F10 && code.code <= keyboard::F12 )
        w << "F1" << char((code.code - keyboard::F10) + '0') << "     ";
    else
        w << "        ";


    w << " 0x" << sc_ascii(code.code);

    if ( type == keyboard::EventType::Down && (code.is_ascii() || code.is_keypad()) )
    {
        char ch = code.code & 0xff;

        if ( !(key_mode & NoEcho) && (ch != '\b' || key_buffer.size() > 0))
        {
            w.move_to_cursor();
            w << ch;
            w.align_cursor();
        }

        if ( key_mode & Unbuffered )
        {
            key_buffer.clear();
            key_buffer.push_back(ch);
            user_input(key_buffer.ro_view());
        }
        else if ( ch == '\n' )
        {
            user_input(key_buffer.ro_view());
            key_buffer.clear();
        }
        else if ( ch == '\b' )
        {
            key_buffer.pop_back();
            if ( !(key_mode & NoEcho) )
            {
                w << ' ';
            }
        }
        else
        {
            key_buffer.push_back(ch);
        }
    }
}
