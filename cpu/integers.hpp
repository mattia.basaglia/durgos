#ifndef CPU_INTEGERS_HPP
#define CPU_INTEGERS_HPP

/**
 * \file
 * Implementations must define the following types in the cpu namespace:
 * * (u)int(8|16|32)_t - which must be of the approbriate size
 * * (u)intmax_t       - biggest integers avaliable
 * * byte              - single byte
 * * word              - word
 * And the following constexpr values (true or false depending on the processor):
 * * little_endian
 * * big_endian
 */

#include "integers_impl.hpp"

namespace cpu {
    inline constexpr uint16_t low_16(uint32_t address) { return address & 0xFFFF; }
    inline constexpr uint16_t high_16(uint32_t address) { return (address >> 16) & 0xFFFF; }
} // namespace cpu



#endif // CPU_INTEGERS_HPP
