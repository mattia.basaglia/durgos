#ifndef CPU_PORTS_HPP
#define CPU_PORTS_HPP

#include "cpu/integers.hpp"


namespace port {
    namespace detail {
        extern "C" {
            /// Implementations must define this
            cpu::byte port_byte_in_impl(cpu::uint16_t port);
            /// Implementations must define this
            void port_byte_out_impl(cpu::uint16_t port, cpu::byte data);
        }
    }

    /**
     * \brief Read a byte from the given port
     **/
    inline cpu::byte byte_in(cpu::uint16_t port)
    {
        return detail::port_byte_in_impl(port);
    }

    /**
     * \brief Writes a character to the given port
     **/
    inline void byte_out(cpu::uint16_t port, cpu::byte data)
    {
        return detail::port_byte_out_impl(port, data);
    }


} // namespace port


#endif // CPU_PORTS_HPP
