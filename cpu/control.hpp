#ifndef CPU_CONTROL_HPP
#define CPU_CONTROL_HPP

namespace cpu {

    /**
     * \brief Implementation should define this so that the CPU is then halted
     */
    void halt();

} // namespace cpu

#endif // CPU_CONTROL_HPP
