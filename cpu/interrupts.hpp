#ifndef CPU_INTERRUPTS_HPP
#define CPU_INTERRUPTS_HPP

#include "interrupt_callback.hpp"
#include <durgos/managed_string.hpp>

namespace cpu {
namespace interrupt {
    /**
     * Implementations must define this with a named version of relevant interrupts
     */
    enum class Interrupt : std::uint8_t;
    /**
     * Implementations must define this structure, which will hold stack information
     */
    struct CallbackArgument;

    using Callback = void (*)(const CallbackArgument&);

    /**
     * \brief Implementations must define this so \p handler is invoked when the
     *        given interrupt is triggered by the CPU
     */
    void register_handler(Interrupt interrupt, Callback handler);

    using ExceptionCallback = void (*)(const CallbackArgument&, durgos::ROString name);

    /**
     * \brief Implementations must define this so \p handler is invoked when the
     *        given exception interrupt is triggered by the CPU and there are no
     *        specific andler registered for it
     */
    void register_exception_handler(ExceptionCallback handler);

    /**
     * \brief Implementations must define this so interrupts are endabled and
     *        directed to the relevant handlers
     */
    void initialize();
} // namespace interrupt
} // namespace cpu

#endif // CPU_INTERRUPTS_HPP
