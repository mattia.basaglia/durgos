#include "cpu/interrupts.hpp"
#include "cpu/ports.hpp"

namespace cpu {
namespace interrupt {

// ISRs reserved for CPU exceptions
extern "C" {
    void isr0();
    void isr1();
    void isr2();
    void isr3();
    void isr4();
    void isr5();
    void isr6();
    void isr7();
    void isr8();
    void isr9();
    void isr10();
    void isr11();
    void isr12();
    void isr13();
    void isr14();
    void isr15();
    void isr16();
    void isr17();
    void isr18();
    void isr19();
    void isr20();
    void isr21();
    void isr22();
    void isr23();
    void isr24();
    void isr25();
    void isr26();
    void isr27();
    void isr28();
    void isr29();
    void isr30();
    void isr31();

    void irq0();
    void irq1();
    void irq2();
    void irq3();
    void irq4();
    void irq5();
    void irq6();
    void irq7();
    void irq8();
    void irq9();
    void irq10();
    void irq11();
    void irq12();
    void irq13();
    void irq14();
    void irq15();
} // extern "C"


// How every interrupt gate (handler) is defined
struct  idt_gate_t
{
    // Lower 16 bits of handler function address
    std::uint16_t low_offset;
    // Kernel segment selector
    std::uint16_t sel;
    std::uint8_t always0;
    /* First byte
     * Bit 7: "Interrupt is present"
     * Bits 6-5: Privilege level of caller (0=kernel..3=user)
     * Bit 4: Set to 0 for interrupt gates
     * Bits 3-0: bits 1110 = decimal 14 = "32 bit interrupt gate" */
    std::uint8_t flags;
    // Higher 16 bits of handler function address
    std::uint16_t high_offset;
} __attribute__((packed));

/* A pointer to the array of interrupt handlers.
 * Assembly instruction 'lidt' will read it */
struct idt_register_t
{
    std::uint16_t limit;
    std::uint32_t base;
} __attribute__((packed));

static const int IDT_ENTRIES = 256;
static idt_gate_t idt[IDT_ENTRIES];
static idt_register_t idt_reg;

// Segment selectors
static const int KERNEL_CS = 0x08;

void set_idt_gate(int n, std::uintptr_t handler)
{
    idt[n].low_offset = cpu::low_16(handler);
    idt[n].sel = KERNEL_CS;
    idt[n].always0 = 0;
    idt[n].flags = 0x8E;
    idt[n].high_offset = cpu::high_16(handler);
}

void set_idt()
{
    idt_reg.base = (std::uintptr_t) &idt;
    idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;
    /* Don't make the mistake of loading &idt -- always load &idt_reg */
    asm volatile("lidtl (%0)" : : "r" (&idt_reg));
}


void initialize()
{
    set_idt_gate(0,  (cpu::uintptr_t)isr0);
    set_idt_gate(1,  (cpu::uintptr_t)isr1);
    set_idt_gate(2,  (cpu::uintptr_t)isr2);
    set_idt_gate(3,  (cpu::uintptr_t)isr3);
    set_idt_gate(4,  (cpu::uintptr_t)isr4);
    set_idt_gate(5,  (cpu::uintptr_t)isr5);
    set_idt_gate(6,  (cpu::uintptr_t)isr6);
    set_idt_gate(7,  (cpu::uintptr_t)isr7);
    set_idt_gate(8,  (cpu::uintptr_t)isr8);
    set_idt_gate(9,  (cpu::uintptr_t)isr9);
    set_idt_gate(10, (cpu::uintptr_t)isr10);
    set_idt_gate(11, (cpu::uintptr_t)isr11);
    set_idt_gate(12, (cpu::uintptr_t)isr12);
    set_idt_gate(13, (cpu::uintptr_t)isr13);
    set_idt_gate(14, (cpu::uintptr_t)isr14);
    set_idt_gate(15, (cpu::uintptr_t)isr15);
    set_idt_gate(16, (cpu::uintptr_t)isr16);
    set_idt_gate(17, (cpu::uintptr_t)isr17);
    set_idt_gate(18, (cpu::uintptr_t)isr18);
    set_idt_gate(19, (cpu::uintptr_t)isr19);
    set_idt_gate(20, (cpu::uintptr_t)isr20);
    set_idt_gate(21, (cpu::uintptr_t)isr21);
    set_idt_gate(22, (cpu::uintptr_t)isr22);
    set_idt_gate(23, (cpu::uintptr_t)isr23);
    set_idt_gate(24, (cpu::uintptr_t)isr24);
    set_idt_gate(25, (cpu::uintptr_t)isr25);
    set_idt_gate(26, (cpu::uintptr_t)isr26);
    set_idt_gate(27, (cpu::uintptr_t)isr27);
    set_idt_gate(28, (cpu::uintptr_t)isr28);
    set_idt_gate(29, (cpu::uintptr_t)isr29);
    set_idt_gate(30, (cpu::uintptr_t)isr30);
    set_idt_gate(31, (cpu::uintptr_t)isr31);


    // Remap the PIC (Programmable interrupt controller)
    port::byte_out(0x20, 0x11);
    port::byte_out(0xA0, 0x11);
    port::byte_out(0x21, 0x20);
    port::byte_out(0xA1, 0x28);
    port::byte_out(0x21, 0x04);
    port::byte_out(0xA1, 0x02);
    port::byte_out(0x21, 0x01);
    port::byte_out(0xA1, 0x01);
    port::byte_out(0x21, 0x0);
    port::byte_out(0xA1, 0x0);

    // Install the IRQs (Interrupt requests
    set_idt_gate(32, (cpu::uintptr_t)irq0);
    set_idt_gate(33, (cpu::uintptr_t)irq1);
    set_idt_gate(34, (cpu::uintptr_t)irq2);
    set_idt_gate(35, (cpu::uintptr_t)irq3);
    set_idt_gate(36, (cpu::uintptr_t)irq4);
    set_idt_gate(37, (cpu::uintptr_t)irq5);
    set_idt_gate(38, (cpu::uintptr_t)irq6);
    set_idt_gate(39, (cpu::uintptr_t)irq7);
    set_idt_gate(40, (cpu::uintptr_t)irq8);
    set_idt_gate(41, (cpu::uintptr_t)irq9);
    set_idt_gate(42, (cpu::uintptr_t)irq10);
    set_idt_gate(43, (cpu::uintptr_t)irq11);
    set_idt_gate(44, (cpu::uintptr_t)irq12);
    set_idt_gate(45, (cpu::uintptr_t)irq13);
    set_idt_gate(46, (cpu::uintptr_t)irq14);
    set_idt_gate(47, (cpu::uintptr_t)irq15);

    set_idt(); // Load with ASM

    // Enable interrupts
    asm volatile("sti");
}

// To print the message which defines every exception
static const char *exception_messages[] =
{
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",

    "Double Fault",
    "Coprocessor Segment Overrun",
    "Invalid Task State Segment",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Reserved",

    "Math Fault",
    "Alignment Check",
    "Machine Check",
    "SIMD Floating-Point Exception",
    "Virtualization Exception",
    "Control Protection Exception",
    "Reserved",
    "Reserved",

    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

Callback interrupt_handlers[256];
ExceptionCallback exception_handler = nullptr;

extern "C" void isr_handler(const CallbackArgument& r)
{
    if (interrupt_handlers[r.int_no] )
        interrupt_handlers[r.int_no](r);
    else if ( exception_handler )
        exception_handler(r, exception_messages[r.int_no]);
}

void register_exception_handler(ExceptionCallback handler)
{
    exception_handler = handler;
}

void register_handler(Interrupt interrupt, Callback handler)
{
    interrupt_handlers[cpu::uint8_t(interrupt)] = handler;
}

extern "C" void irq_handler(const CallbackArgument& r)
{
    /* After every interrupt we need to send an EOI to the PICs
     * or they will not send another interrupt again */
    if ( r.int_no >= 40 )
        port::byte_out(0xA0, 0x20); /* slave */
    port::byte_out(0x20, 0x20); /* master */

    // Handle the interrupt in a more modular way
    if ( interrupt_handlers[r.int_no] )
        interrupt_handlers[r.int_no](r);
}

} // namespace interrupt
} // namespace cpu
