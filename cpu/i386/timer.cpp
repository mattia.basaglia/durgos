#include "cpu/timer.hpp"
#include "cpu/ports.hpp"

namespace cpu {
namespace timer {

static Callback high_level_callback;

static void timer_callback(const interrupt::CallbackArgument&)
{
    high_level_callback();
}

void register_callback(std::uint32_t freq, Callback callback)
{
    high_level_callback = callback;

    interrupt::register_handler(interrupt::Interrupt::SystemTimer, timer_callback);

    // Get the PIT value: hardware clock at 1193182 Hz
    std::uint32_t divisor = 1193182 / freq;
    std::uint8_t low  = (std::uint8_t)(divisor & 0xFF);
    std::uint8_t high = (std::uint8_t)((divisor >> 8) & 0xFF);
    // Send the command
    port::byte_out(0x43, 0x36); // Command port
    port::byte_out(0x40, low);
    port::byte_out(0x40, high);
}


} // namespace timer
} // namespace cpu