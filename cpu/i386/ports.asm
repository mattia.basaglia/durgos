; i386 implementation of port I/O

global port_byte_in_impl
port_byte_in_impl:
    ; push stack
    push ebp
    mov ebp, esp
    ; get the argument
    mov edx, [ebp+8]
    ; port input (reads dx as port, writes al)
    in al, dx
    ; pop stack and return
    leave
    ret

global port_byte_out_impl
port_byte_out_impl:
    ; push stack
    push ebp
    mov ebp, esp
    ; get the arguments
    mov edx, [ebp+8]
    mov al, [ebp+12]
    ; port output (reads dx as port, al as data)
    out dx, al
    ; pop stack and return
    leave
    ret
