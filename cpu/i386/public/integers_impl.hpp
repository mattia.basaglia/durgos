#ifndef I368_STDINT_HPP
#define I368_STDINT_HPP

namespace cpu {

using uint32_t = unsigned int;
using int32_t = signed int;
using uint16_t = unsigned short;
using int16_t = signed short;
using uint8_t = unsigned char;
using int8_t = signed char;

using intmax_t = long long;
using uintmax_t = unsigned long long;

using uint64_t = unsigned long long;
using int64_t = signed long long;

using byte = uint8_t;
using word = uint16_t;

using intptr_t = int32_t;
using uintptr_t = uint32_t;

static_assert(sizeof(uint64_t) == 8, "Wrong integer size");
static_assert(sizeof(uint32_t) == 4, "Wrong integer size");
static_assert(sizeof(uint16_t) == 2, "Wrong integer size");
static_assert(sizeof(uint8_t) == 1, "Wrong integer size");
static_assert(sizeof(uintptr_t) == sizeof(void*), "Wrong pointer size");

static constexpr bool little_endian = true;
static constexpr bool big_endian = false;
} // namespace cpu
#endif // I368_STDINT_HPP
