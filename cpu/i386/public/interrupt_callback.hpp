#ifndef I386_ISR_HPP
#define I386_ISR_HPP

#include <cstdint>


namespace cpu {
namespace interrupt {

// Struct which aggregates many registers
struct CallbackArgument
{
    // Data segment selector
    std::uint32_t ds;
    // Pushed by pusha.
    std::uint32_t edi, esi, ebp, unused, ebx, edx, ecx, eax;
    // Interrupt number and error code (if applicable)
    std::uint32_t int_no, err_code;
    // Pushed by the processor automatically
    std::uint32_t eip, cs, eflags, esp, ss;
};

/**
 * \brief Interrupt constants
 */
enum class Interrupt : std::uint8_t
{
    DivisionByZero = 0,
    IRQ0  = 32,
    SystemTimer = IRQ0,
    IRQ1  = 33,
    Keyboard = IRQ1,
    IRQ2  = 34,
    IRQ3  = 35,
    IRQ4  = 36,
    IRQ5  = 37,
    IRQ6  = 38,
    IRQ7  = 39,
    IRQ8  = 40,
    IRQ9  = 41,
    IRQ10 = 42,
    IRQ11 = 43,
    IRQ12 = 44,
    IRQ13 = 45,
    IRQ14 = 46,
    IRQ15 = 47,
};

} // namespace cpu
} // namespace interrupt

#endif // I386_ISR_HPP

