#include "cpu/control.hpp"

void cpu::halt()
{
    asm volatile("hlt");
}
