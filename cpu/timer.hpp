#ifndef I386_TIMER_HPP
#define I386_TIMER_HPP

#include "cpu/integers.hpp"
#include "cpu/interrupts.hpp"

namespace cpu {
namespace timer {
    using Callback = void (*)();
    /**
     * \brief Implementations must define this so the callback is called with
     *        the given frequency
     */
    void register_callback(cpu::uint32_t freq, Callback callback);
} // namespace timer
} // namespace cpu

#endif // I386_TIMER_HPP
