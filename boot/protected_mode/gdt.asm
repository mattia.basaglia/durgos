; Set the global descriptor table
gdt_start:
    ; the GDT starts with a null 8-byte
    dd 0x0 ; 4 byte
    dd 0x0 ; 4 byte
    ; Could also use dq to write this

; GDT for code segment. base = 0x00000000, length = 0xfffff
; for flags, refer to os-dev.pdf document, page 36
gdt_code:
    dw 0xffff    ; segment length, bits 0-15
    dw 0x0       ; segment base, bits 0-15
    db 0x0       ; segment base, bits 16-23
    ; flags (8 bits)
    ; present       [P]     : 1 (this segment is present in memory)
    ; privilege     [DPL]   : 00 (highest)
    ; desc type     [S]     : 1 (code or data, 0 for traps)
    ; TYPE
    ;   code                : 1 (code segment)
    ;   conforming          : 0 (can't call lower privilege segments)
    ;   readable            : 1 (0 for exectute-only)
    ;   accessed            : 0 (CPU sets it when accessing the segment)
    db 10011010b
    ; Other flags (4 bits) + Seg Limit
    ; Granularity   [G]     : 1 multiply limit by 4K
    ; Oper. size    [D/B]   : 1 32 bit segment (0 for 16 bit)
    ; 64 bits       [L]     : 0 (using 32 bits)
    ; Avail. to sys [AVL]   : 0 (can be used for debugging?)
    ; Seg. Limit
    db 11001111b ; flags (4 bits) + segment length, bits 16-19
    db 0x0       ; segment base, bits 24-31

; GDT for data segment. base and length identical to code segment
; some flags changed, again, refer to os-dev.pdf
gdt_data:
    dw 0xffff
    dw 0x0
    db 0x0
    ; Changed TYPE code to 0 for data segment
    db 10010010b
    db 11001111b
    db 0x0

gdt_end: ; label used to calculate size

; GDT descriptor
gdt_descriptor:
    dw gdt_end - gdt_start - 1 ; size (16 bit), always one less of its true size
    dd gdt_start ; address (32 bit)

; define some constants for later use
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start
