bits 16

org 0x7c00

; load kernel sectors
    mov ah, 0x42 ; set to lba extended read
    mov dl, 0x80 ; first hard drive
    mov si, dap  ; address of disk address package
    int 0x13     ; execute

    jc load_failed ; do something on failure

    mov bx, MSG_LOAD_OK
    call print
    call print_nl

; call kernel entry
    jmp KERNEL_OFFSET ; jump to the destination address
    jmp $


load_failed:
    mov bx, MSG_LOAD_KO
    call print
    call print_nl
    jmp $


; this is the disk address package used by the lba extensions
dap:
    db 0x10             ; size of this package
    db 0x0              ; reserved
    dw KERNEL_SIZE      ; number of sectors to load
    dd KERNEL_OFFSET    ; destination address
    dq 0x1              ; sector index to load

%include "functions/print.asm"

MSG_LOAD_OK db "Kernel loaded", 0
MSG_LOAD_KO db "Kernel loading failed", 0


; pad + boot signature
times 510 - ($ - $$) db 0
dw 0xaa55
