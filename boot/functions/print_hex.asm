; prints dx as a 2 byte hex string

print_hex:
    pusha

    mov ah, 0x0e
    mov al, '0'
    int 0x10
    mov al, 'x'
    int 0x10

    mov cx, 0
    body:
        rol dx, 4       ; rotate left by 4 bits to make sure we process them in order
        mov ax, dx
        and ax, 0xf
        add ax, '0'
        cmp ax, '9'
        jle decd
        add ax, 0x7 ; difference between '9' and 'A'
        decd:
        mov ah, 0x0e
        int 0x10
        inc cx
        cmp cx, 4
        jne body

    popa
    ret


;
; ; prints eax
; print_dword:
;     push eax
;     ror eax, 16
;     call print_word
;     pop eax
;     call print_word
;     ret
;
; ; prints ax
; print_word:
;     push ax
;     mov al, ah
;     call print_byte
;     pop ax
;     call print_byte
;     ret
;
; ; Prints al
; print_byte:
;     push ax
;     ror al, 4
;     call pb_digit
;     pop ax
;     call pb_digit
;     ret
; ; Prints the last 4 bits of al
; pb_digit:
;     and al, 0xf
;     add al, '0'
;     cmp al, '9'
;     jle pb_print
;     add al, 'A' - 10 - '0'
; pb_print:
;     mov ah, 0x0e ; tty mode
;     int 0x10 ; print
;     ret
