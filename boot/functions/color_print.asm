; Print with colors, [ebx] must point to the address to use, ah should be the color

[bits 32] ; using 32-bit protected mode

; this is how constants are defined
VIDEO_MEMORY equ 0xb8000

BG_BLACK            equ 0x00
BG_BLUE             equ 0x10
BG_GREEN            equ 0x20
BG_CYAN             equ 0x30
BG_RED              equ 0x40
BG_MAGENTA          equ 0x50
BG_YELLOW           equ 0x60
BG_WHITE            equ 0x70
BG_BRIGHT           equ 0x80
BG_BLACK_BRIGHT     equ 0x80
BG_BLUE_BRIGHT      equ 0x90
BG_GREEN_BRIGHT     equ 0xa0
BG_CYAN_BRIGHT      equ 0xb0
BG_RED_BRIGHT       equ 0xc0
BG_MAGENTA_BRIGHT   equ 0xd0
BG_YELLOW_BRIGHT    equ 0xe0
BG_WHITE_BRIGHT     equ 0xf0

FG_BLACK            equ 0x00
FG_BLUE             equ 0x01
FG_GREEN            equ 0x02
FG_CYAN             equ 0x03
FG_RED              equ 0x04
FG_MAGENTA          equ 0x05
FG_YELLOW           equ 0x06
FG_WHITE            equ 0x07
FG_BRIGHT           equ 0x08
FG_BLACK_BRIGHT     equ 0x08
FG_BLUE_BRIGHT      equ 0x09
FG_GREEN_BRIGHT     equ 0x0a
FG_CYAN_BRIGHT      equ 0x0b
FG_RED_BRIGHT       equ 0x0c
FG_MAGENTA_BRIGHT   equ 0x0d
FG_YELLOW_BRIGHT    equ 0x0e
FG_WHITE_BRIGHT     equ 0x0f

color_print:
    pusha
    mov edx, VIDEO_MEMORY

color_print_loop:
    mov al, [ebx] ; [ebx] is the address of our character

    cmp al, 0 ; check if end of string
    je color_print_done

    mov [edx], ax ; store character + attribute in video memory
    add ebx, 1 ; next char
    add edx, 2 ; next video memory position

    jmp color_print_loop

color_print_done:
    popa
    ret
