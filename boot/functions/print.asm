; Prints the NUL-terminated string at [bx]

print:
    pusha

print_loop:
    mov al, [bx]
    cmp al, 0
    jz print_end
    mov ah, 0x0e ; tty mode
    int 0x10 ; print
    add bx, 1
    jmp print_loop

print_end:
    popa
    ret

print_nl:
    pusha
    mov ax, 0x0e0a ; tty + newline char
    int 0x10
    mov al, 0x0d ; carriage return
    int 0x10
    popa
    ret
