bits 16

; Switch to protected mode
mov bx, MSG_PROT_MODE
call print
call print_nl
call switch_to_pm ; disable interrupts, load GDT,  etc. Finally jumps to 'BEGIN_PM'
jmp $

%include "functions/print.asm"
%include "protected_mode/gdt.asm"
%include "protected_mode/enter.asm"

MSG_PROT_MODE db "Switching to protected mode", 0


bits 32
BEGIN_PM:
; _start is for the linker
global _start;
_start:
; invoke kernel function
    extern main
    call main
    jmp $
