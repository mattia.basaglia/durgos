#include "keyboard.hpp"
#include "cpu/ports.hpp"
#include "cpu/interrupts.hpp"

namespace keyboard {

// see also http://www.win.tue.nl/%7Eaeb/linux/kbd/scancodes-1.html
static std::uint16_t scan_codes_uk[] = {
    Error,

    Escape,

    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    '-',
    '=',
    Backspace,

    Tab,
    'q',
    'w',
    'e',
    'r',
    't',
    'y',
    'u',
    'i',
    'o',
    'p',
    '[',
    ']',
    Enter,

    LCtrl,
    'a',
    's',
    'd',
    'f',
    'g',
    'h',
    'j',
    'k',
    'l',
    ';',
    '\'',
    '`',

    LShift,
    '#',
    'z',
    'x',
    'c',
    'v',
    'b',
    'n',
    'm',
    ',',
    '.',
    '/',
    RShift,

    Keypad | '*',
    LAlt,
    ' ',
    CapsLock,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,

    NumLock,
    ScrollLock,
    Keypad | '7',
    Keypad | '8',
    Keypad | '9',
    Keypad | '-',
    Keypad | '4',
    Keypad | '5',
    Keypad | '6',
    Keypad | '+',
    Keypad | '1',
    Keypad | '2',
    Keypad | '3',
    Keypad | '0',
    Keypad | '.',

    Unknown | 0x54,
    Unknown | 0x55,
    Unknown | 0x56,
    F11,
    F12,
};

static constexpr int nscan_codes_uk = sizeof(scan_codes_uk) / sizeof(scan_codes_uk[0]);


std::uint16_t get_key_code(cpu::uint16_t scancode)
{
    if ( scancode & 0xff00 )
    {
        if ( scancode == PrintScreen || scancode == Menu || scancode == LMeta || scancode == RMeta )
            return scancode;
        auto sub_code = get_key_code(scancode & 0xff);
        if ( sub_code == '/' || sub_code == Enter )
            return sub_code | Keypad;

        if ( sub_code == LCtrl || sub_code == LAlt || (sub_code & Keypad) )
            return scancode;
    }
    else
    {
        if ( scancode > 0x80 )
            scancode -= 0x80;
        if ( scancode < nscan_codes_uk )
        return scan_codes_uk[scancode];
    }

    return scancode | Unknown;
}

std::uint8_t previous = 0;

static KeyboardCallback outer_callback;
void foo( EventType){}

static void keyboard_callback(const cpu::interrupt::CallbackArgument&)
{
    // The PIC leaves us the scancode in port 0x60
    cpu::uint8_t scancode = port::byte_in(0x60);

    if ( scancode == 0xe0 || scancode == 0xe1 )
    {
        previous = scancode;
        return;
    }

    EventType type = EventType::Down;

    if ( scancode >= 0x80 )
    {
        type = EventType::Up;
    }

    cpu::uint16_t full_scancode =  (previous << 8) | scancode;
    previous = 0;
    outer_callback(KeyCode(get_key_code(full_scancode), full_scancode), type);
}

void register_callback(KeyboardCallback callback)
{
    outer_callback = callback;
    cpu::interrupt::register_handler(cpu::interrupt::Interrupt::Keyboard, keyboard_callback);
}

} // namespace keyboard

