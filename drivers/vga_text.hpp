#ifndef DRIVERS_VGA_HPP
#define DRIVERS_VGA_HPP

#include <cstdint>
#include <durgos/string.hpp>
#include "kernel/tty.hpp"

namespace vga {


namespace Register {
    static const int SCREEN_CTRL = 0x3d4;
    static const int SCREEN_DATA = 0x3d5;
} // namespace Register

/// mode 0x03
class TextMode
{
public:
    struct Character
    {
        char text;
        std::uint8_t color;
    };
    using buffer_pointer = Character*;
    static const std::uintptr_t buffer_pos = 0xb8000;
    static const int screen_columns = 80;
    static const int screen_rows = 25;
    static const int character_width = 1;
    static const int character_height = 1;

    inline static buffer_pointer buffer()
    {
        return buffer_pointer(buffer_pos);
    }

    /**
     * \brief Fetch the cursor offset from the VGA registers
     */
    static void set_cursor(std::size_t offset);

    /**
     * \brief Set the VGA cursor offset
     */
    static std::size_t get_cursor();

    static void fill_screen(tty::Style style)
    {
        std::uint8_t byte = style.to_byte();
        for ( int i = 0; i < screen_rows * screen_columns; i++ )
            buffer()[i] = Character{' ', byte};
    }

    static void write_char(std::size_t offset, char c, tty::Style style)
    {
        buffer()[offset] = Character{c, style.to_byte()};
    }

    static void copy_block(std::size_t offet_from, std::size_t offet_to, std::size_t count)
    {
        durgos::pod_copy(buffer() + offet_from, buffer() + offet_to, count);
    }

    static constexpr tty::TtyDriver driver()
    {
        return {
            character_width,
            character_height,
            screen_rows,
            screen_columns,
            &TextMode::set_cursor,
            &TextMode::get_cursor,
            &TextMode::fill_screen,
            &TextMode::copy_block,
            &TextMode::write_char
        };
    }
};

/// mode 0x13
class VideoMode
{
public:
    using buffer_pointer = std::uint8_t*;
    static const std::uintptr_t buffer_pos = 0xA0000;
    static const int screen_columns = 320;
    static const int screen_rows = 200;
    static const int character_width = 8;
    static const int character_height = 8;


    inline static buffer_pointer buffer()
    {
        return buffer_pointer(buffer_pos);
    }

    static void fill_screen(tty::Style style)
    {
        for ( int i = 0; i < screen_rows * screen_columns; i++ )
            buffer()[i] = style.background;
    }

    static void write_char(std::size_t offset, char c, tty::Style style);

    static void copy_block(std::size_t offet_from, std::size_t offet_to, std::size_t count)
    {
        durgos::pod_copy(buffer() + offet_from, buffer() + offet_to, count);
    }

    static constexpr tty::TtyDriver driver()
    {
        return {
            character_width,
            character_height,
            screen_rows,
            screen_columns,
            &TextMode::set_cursor,
            &TextMode::get_cursor,
            &VideoMode::fill_screen,
            &VideoMode::copy_block,
            &VideoMode::write_char
        };
    }
};

} // namespace vga

#endif // DRIVERS_VGA_HPP
