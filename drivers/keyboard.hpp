#ifndef DRIVER_KEYBOARD
#define DRIVER_KEYBOARD

#include "cpu/interrupts.hpp"

namespace keyboard {

enum KeyCodeEnum : std::uint16_t
{
    Error = 0,
    Escape = 0x1b,
    Backspace = '\b',
    Tab = '\t',
    Enter = '\n',

    LCtrl = 0x0100, // above all ascii characters
    LShift,
    RShift,
    LAlt,
    CapsLock,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    NumLock,
    ScrollLock,

// Just using the scan codes
    PrintScreen = 0xe037,
    RCtrl = 0xe01d,
    RAlt = 0xe038,

    LMeta = 0xe05b,
    RMeta = 0xe05c,
    Menu = 0xe05d,

    // Note equivalent to the Keypad scan codes if not for e0
    Insert = 0xe052,
    Home = 0xe047,
    PageUp = 0xe049,
    Delete = 0xe053,
    End = 0xe04f,
    PageDown = 0xe051,
    ArrowUp = 0xe048,
    ArrowLeft = 0xe04b,
    ArrowDown = 0xe050,
    ArrowRight = 0xe04d,

// Mask
    Keypad = 0x1000, // keypad mask
    Unknown = 0x0200, // unknown mask

};


struct KeyCode
{
    std::uint16_t code;
    std::uint16_t scancode;

    constexpr KeyCode(std::uint16_t code, std::uint16_t scancode)
        : code(code), scancode(scancode) {}

    constexpr bool is_ascii() const
    {
        return (code & 0xff00) == 0;
    }

    constexpr bool is_keypad() const
    {
        return code & Keypad;
    }

    constexpr bool is_error() const
    {
        return code == Error;
    }

    constexpr bool is_unknown() const
    {
        return code & Unknown;
    }
};

enum class EventType
{
    Down,
    Up,
};

using KeyboardCallback = void(*)(KeyCode, EventType);

void register_callback(KeyboardCallback callback);

} // namespace keyboard

#endif // DRIVER_KEYBOARD
