#include "drivers/vga_text.hpp"

#include "cpu/ports.hpp"

namespace vga {

std::size_t TextMode::get_cursor()
{
    // Screen cursor position: ask VGA control register (0x3d4) for bytes
    // 14 = high byte of cursor and 15 = low byte of cursor. */
    port::byte_out(Register::SCREEN_CTRL, 14);

    // Data is returned in VGA data register (0x3d5)
    std::size_t position = port::byte_in(Register::SCREEN_DATA);
    position = position << 8; // high byte

    // requesting low byte
    port::byte_out(Register::SCREEN_CTRL, 15);
    position |= port::byte_in(Register::SCREEN_DATA);

    return position;
}

/**
 * \brief Set the VGA cursor offset
 */
void TextMode::set_cursor(std::size_t offset)
{
    /* Similar to get_cursor_offset, but instead of reading we write data */
    port::byte_out(Register::SCREEN_CTRL, 14);
    port::byte_out(Register::SCREEN_DATA, (offset >> 8) & 0xff);
    port::byte_out(Register::SCREEN_CTRL, 15);
    port::byte_out(Register::SCREEN_DATA, offset & 0xff);
}



} // namespace vga
