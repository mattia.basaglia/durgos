ROOT_PATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

ARCH=i386
QEMU=qemu-system-$(ARCH)
BIN_PATH=$(ROOT_PATH)xcompiler/bin
export PATH := $(BIN_PATH):$(PATH)
ASM=nasm
ASMFLAGS=-I $(ROOT_PATH)boot/
CC=$(ARCH)-elf-gcc
CFLAGS = -ffreestanding -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles -lgcc
CFLAGS += -g -Wall -Wextra
CXX=$(ARCH)-elf-g++
CXXFLAGS = --std=c++17 -fno-exceptions -fno-rtti
LD=$(ARCH)-elf-ld
LDFLAGS=
GDB=$(ARCH)-elf-gdb
AR=$(ARCH)-elf-ar
ARFLAGS=rcs
IMAGE=out/kernel.image
INCLUDE=-I $(ROOT_PATH) -I $(ROOT_PATH)cpu/$(ARCH)/public -I $(ROOT_PATH)lib/libc/include -I $(ROOT_PATH)lib/durgoslib/include

# Boot sector is at 0x7c00 and is 512 bytes long (0x200) so the next sector is at 0x7e00
KERNEL_OFFSET=0x7e00
CB=)
ASM_SOURCES = $(wildcard cpu/$(ARCH)/*.asm)
SOURCES = $(wildcard 			\
	kernel/*.cpp			\
	drivers/*.cpp 			\
	cpu/$(ARCH)/*.cpp 		\
)
HEADERS = $(wildcard			\
	kernel/*.hpp			\
	kernel/*/*.hpp			\
	drivers/*.hpp			\
	cpu/$(ARCH)/*.hpp		\
	cpu/$(ARCH)/*/*.hpp		\
	cpu/*.hpp			\
	lib/libc/include/*		\
	lib/libc/include/*/*		\
	lib/durgoslib/include/durgos/*	\
)

OBJS = ${SOURCES:%.cpp=out/%.o} ${ASM_SOURCES:%.asm=out/%.o}

LIBC_SOURCES = $(wildcard lib/libc/src/*)
LIBC_HEADERS = $(wildcard lib/libc/include/*)
LIBC_OBJS = ${LIBC_SOURCES:%.cpp=out/%.o}

DUSGOSLIB_SOURCES = $(wildcard lib/durgoslib/src/*)
DUSGOSLIB_HEADERS = $(wildcard lib/durgoslib/include/*)
DUSGOSLIB_OBJS = ${DUSGOSLIB_SOURCES:%.cpp=out/%.o}

.SUFFIXES:

.PHONY: run all compiler debug debug_start

all: $(IMAGE)

out/%.bin: %.asm
	mkdir -p $(dir $@)
	$(ASM) $(ASMFLAGS) -f bin $< -o $@

out/%.o: %.asm
	mkdir -p $(dir $@)
	$(ASM) $(ASMFLAGS) -f elf $< -o $@

out/%.o: %.cpp $(HEADERS)
	mkdir -p $(dir $@)
	$(CXX) $(INCLUDE) $(CFLAGS) $(CXXFLAGS) -c $< -o $@
# 	Freestanding means no stdlib and no main()

%.bin: %.o
	$(LD) -Ttext 0x0 --oformat binary $< -o $@

out/boot/bootsector.bin: out/kernel.bin
out/boot/bootsector.bin: shex_size = $$(( ($$(stat -c %s out/kernel.bin)+511)/512 ))
out/boot/bootsector.bin: ASMFLAGS += -DKERNEL_SIZE=$(shell echo $(shex_size)) -DKERNEL_OFFSET=$(KERNEL_OFFSET)
out/boot/bootsector.bin: boot/functions/print.asm

out/boot/kernel-entry.bin: boot/functions/print.asm
out/boot/kernel-entry.bin: boot/protected_mode/gdt.asm
out/boot/kernel-entry.bin: boot/protected_mode/enter.asm

out/kernel.image: out/boot/bootsector.bin out/kernel.bin
	cat $^ > $@

out/kernel.bin: out/kernel.elf
	objcopy -O binary $< $@

out/kernel.elf: out/boot/kernel-entry.o $(OBJS) out/libc.a
	$(LD) $(LDFLAGS) -Ttext $(KERNEL_OFFSET) $^ -o $@

out/libc.a: $(LIBC_OBJS) $(DUSGOSLIB_OBJS)
	$(AR) $(ARFLAGS) $@ $^

run: $(IMAGE)
	$(QEMU) -drive format=raw,file=$(IMAGE)

debug_start: $(IMAGE) $(IMAGE:.image=.elf)
	$(QEMU) -s -S -drive format=raw,file=$(IMAGE) &

debug: debug_start
	$(GDB) -q -ex "target remote localhost:1234" -ex "symbol-file $(IMAGE:.image=.elf)"

clean:
	rm -rf out

compiler:
	cd $(ROOT_PATH)xcompiler && make

out/durgos.bin: boot/kernel.ld out/multiboot.o $(OBJS) out/libc.a
	$(CC) -T boot/kernel.ld -o $@ -ffreestanding -O2 -nostdlib out/multiboot.o $(OBJS) out/libc.a

out/durgos.iso: out/iso/boot/grub/grub.cfg out/iso/boot/durgos.bin
	grub-mkrescue -o $@ out/iso

out/iso/boot/grub/grub.cfg: boot/grub.cfg
	mkdir -p out/iso/boot/grub/
	cp $< $@

out/iso/boot/durgos.bin: out/durgos.bin
	mkdir -p out/iso/boot/
	cp $< $@

out/multiboot.o: boot/multiboot.asm
	$(ASM) $(ASMFLAGS) -felf32 $^ -o $@
